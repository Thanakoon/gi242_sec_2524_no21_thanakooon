﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundM : MonoBehaviour
{
    public static AudioClip PlayerBullet, EnemyBullet, Dead, Theme, Rock;
    static AudioSource AS;
    // Start is called before the first frame update
    void Start()
    {
        PlayerBullet = Resources.Load<AudioClip>("PlayerBullet");
        EnemyBullet = Resources.Load<AudioClip>("EnemyBullet");
        Dead = Resources.Load<AudioClip>("Dead");
        Theme = Resources.Load<AudioClip>("Theme");
        Rock = Resources.Load<AudioClip>("Rock");
        AS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public static void Playsound(string clip)
    {

        switch (clip)
        {
            case "PlayerBullet":
                AS.PlayOneShot(PlayerBullet);
                break;
            case "EnemyBullet":
                AS.PlayOneShot(EnemyBullet);
                break;

            case "Dead":
                AS.PlayOneShot(Dead);
                break;
            case "Theme":
                AS.PlayOneShot(Theme);
                break;
            case "Rock":
                AS.PlayOneShot(Rock);
                break;






        }
    }
    public static void StopSound()
    {
        AS.Stop();
        Debug.Log("Stop");
    }
}
