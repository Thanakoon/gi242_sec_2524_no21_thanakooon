using System;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
       

        private float timer = 0.0f;
        float spawnerRate = 1;
        public event Action OnExploded;
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }
        void Update()
        {
            timer += Time.deltaTime;
            if (timer >= spawnerRate)
            {
                Fire();
                timer = 0.0f;
            }
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            SoundM.Playsound("Dead");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
            SoundM.Playsound("Enemybullet");
        }
    }
}