﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;

namespace Enemy
{

    
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private float chasingThresholdDistance;
     
        private void Update()
        {
            MoveToPlayer();
        }

         private void MoveToPlayer()
         {
            GameObject Player = GameObject.FindGameObjectWithTag("Player");
            transform.position = Vector2.Lerp
               (transform.position, Player.transform.position, 1 * Time.deltaTime);
          
        }
    }    
}

