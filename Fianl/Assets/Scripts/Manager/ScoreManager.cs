﻿using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : monoSingleton<ScoreManager>
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        int countScore;

      
        
        public void Init()
        {
            
            GameManager.Instance.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            scoreText.text = $"Score : {score}";
            countScore = score;
            if (countScore == 5)
            {
                GameManager.Instance.NextLevel();
            }
        }
        
        private void Awake()
        {
            Debug.Assert(scoreText != null, "scoreText cannot null");
        }
        
        private void OnRestarted()
        {

            GameManager.Instance.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
            
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
      




    }
}


