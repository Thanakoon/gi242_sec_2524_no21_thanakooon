﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class GameManager : monoSingleton<GameManager>
    {
        [SerializeField] private Button startButton;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;
      
        public event Action OnRestarted;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        float respawnTime = 0.00f;
        bool starGame = false;
        public int plusScore = 0;
        float limitTime = 8;
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            StartGame();
        }

        private void StartGame()
        {
            ScoreManager.Instance.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            starGame = true;
            SoundM.Playsound("Theme");
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            var spaceship = Instantiate(enemySpaceship);
            spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
            spaceship.OnExploded += OnEnemySpaceshipExploded;
           


        }

        private void OnEnemySpaceshipExploded()
        {
            
            plusScore += 1;
            ScoreManager.Instance.SetScore(plusScore);
            
        }

        private void Restart()
        {
            dialog.gameObject.SetActive(true);
            OnRestarted?.Invoke();
        }
        private void Update()
        {
            
            if (starGame == true)
            {
                respawnTime += Time.deltaTime;
          
            }
            if (respawnTime >= limitTime)
            {
                SpawnEnemySpaceship();
                respawnTime = 0;
            }
        }
        public void NextLevel()
        {
            SoundM.StopSound();
            SoundM.Playsound("Rock");
            limitTime = 4;
        }





    }
}
